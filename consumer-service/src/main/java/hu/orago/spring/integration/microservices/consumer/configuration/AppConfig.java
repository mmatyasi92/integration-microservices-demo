package hu.orago.spring.integration.microservices.consumer.configuration;

import hu.orago.spring.integration.microservices.consumer.model.PostPublishedEvent;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.json.JsonToObjectTransformer;
import org.springframework.integration.redis.inbound.RedisQueueMessageDrivenEndpoint;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.PollableChannel;

@Configuration
@EnableIntegration
@IntegrationComponentScan
public class AppConfig {

    @Bean
    public PollableChannel eventChannel() {
        return new QueueChannel();
    }

    @Bean
    public MessageChannel eventChannelJson() {
        return new DirectChannel();
    }

    @Bean
    @Qualifier("event-inbound-channel-adapter")
    RedisQueueMessageDrivenEndpoint redisQueueMessageDrivenEndpoint(@Qualifier("redisConnectionFactory") RedisConnectionFactory redisConnectionFactory, @Qualifier("serializer") StringRedisSerializer serializer) {
        RedisQueueMessageDrivenEndpoint endpoint =
                new RedisQueueMessageDrivenEndpoint("my-event-queue", redisConnectionFactory);
        endpoint.setAutoStartup(true);
        endpoint.setOutputChannelName("eventChannelJson");
        endpoint.setReceiveTimeout(5000);
        endpoint.setSerializer(serializer);
        return endpoint;
    }

    @Bean
    @Qualifier("serializer")
    public StringRedisSerializer serializer() {
        return new StringRedisSerializer();
    }

    @Bean
    @Transformer(inputChannel = "eventChannelJson", outputChannel = "eventChannel")
    public JsonToObjectTransformer transformer() {
        return new JsonToObjectTransformer(PostPublishedEvent.class);
    }

}
