package hu.orago.spring.integration.microservices.producer.configuration;

import org.reactivestreams.Subscription;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.json.ObjectToJsonTransformer;
import org.springframework.integration.redis.config.RedisQueueOutboundChannelAdapterParser;
import org.springframework.integration.redis.inbound.RedisQueueMessageDrivenEndpoint;
import org.springframework.integration.redis.outbound.RedisQueueOutboundChannelAdapter;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.core.DestinationResolutionException;
import org.springframework.messaging.core.DestinationResolver;

@ImportResource("classpath:WEB-INF/integration-beans.xml")
@AutoConfigureAfter(RedisConfig.class)
@Configuration
@IntegrationComponentScan
@EnableIntegration
public class AppConfig {

    @Bean
    public MessageChannel eventChannel() {
        return new DirectChannel();
    }

    @Bean
    public MessageChannel eventChannelJson() {
        return new DirectChannel();
    }

//    @Bean
//    @Qualifier("event-outbound-channel-adapter")
//    public RedisQueueOutboundChannelAdapter channelAdapter(@Qualifier("redisConnectionFactory") RedisConnectionFactory redisConnectionFactory,
//                                                           @Qualifier("serializer") StringRedisSerializer serializer) {
//        RedisQueueOutboundChannelAdapter adapter = new RedisQueueOutboundChannelAdapter("my-event-queue", redisConnectionFactory);
//        adapter.setSerializer(serializer);
//
//        return adapter;
//    }

//    @Bean
//    @Qualifier("event-outbound-channel-adapter")
//    public RedisQueueMessageDrivenEndpoint redisQueueMessageDrivenEndpoint(@Qualifier("redisConnectionFactory") RedisConnectionFactory redisConnectionFactory,
//                                                                           @Qualifier("serializer") StringRedisSerializer serializer) {
//        RedisQueueMessageDrivenEndpoint endpoint =
//                new RedisQueueMessageDrivenEndpoint("my-event-queue", redisConnectionFactory);
//        endpoint.setAutoStartup(true);
//        endpoint.setOutputChannelName("eventChannelJson");
//        endpoint.setReceiveTimeout(5000);
//        endpoint.setSerializer(serializer);
//        return endpoint;
//    }

    @Bean
    @Qualifier("serializer")
    public StringRedisSerializer serializer() {
        return new StringRedisSerializer();
    }

    @Bean
    @Transformer(inputChannel = "eventChannel", outputChannel = "eventChannelJson")
    public ObjectToJsonTransformer transformer() {
        return new ObjectToJsonTransformer();
    }

}
