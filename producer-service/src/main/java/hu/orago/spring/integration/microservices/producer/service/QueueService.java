package hu.orago.spring.integration.microservices.producer.service;

import hu.orago.spring.integration.microservices.producer.model.PostPublishedEvent;

public interface QueueService {

    void enqueue(PostPublishedEvent event);
}
