package hu.orago.spring.integration.microservices.producer.service;

import hu.orago.spring.integration.microservices.producer.gateway.RedisChannelGateway;
import hu.orago.spring.integration.microservices.producer.model.PostPublishedEvent;
import org.springframework.stereotype.Service;

@Service
public class RedisQueueService implements QueueService {

    private RedisChannelGateway redisChannelGateway;

    public RedisQueueService(RedisChannelGateway redisChannelGateway) {
        this.redisChannelGateway = redisChannelGateway;
    }

    @Override
    public void enqueue(PostPublishedEvent event) {
        redisChannelGateway.enqueue(event);
    }
}

