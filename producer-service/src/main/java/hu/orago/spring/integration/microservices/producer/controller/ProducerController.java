package hu.orago.spring.integration.microservices.producer.controller;

import hu.orago.spring.integration.microservices.producer.model.PostPublishedEvent;
import hu.orago.spring.integration.microservices.producer.service.RedisQueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@RestController
public class ProducerController {

    @Autowired
    private RedisQueueService redisQueueService;

    @PostMapping("/send-event")
    public void send() {
        redisQueueService.enqueue(PostPublishedEvent.builder()
                .emails(Collections.singletonList("mark.matyasi92@gmail.com"))
                .postTitle("test post")
                .postUrl("test uri")
                .build());
    }
}
