package hu.orago.spring.integration.microservices.producer.gateway;

import hu.orago.spring.integration.microservices.producer.model.PostPublishedEvent;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.GatewayHeader;
import org.springframework.integration.annotation.MessagingGateway;

@MessagingGateway
public interface RedisChannelGateway {

    @Gateway(requestChannel="eventChannel", headers = {@GatewayHeader(name = "topic", value = "queue")})
    void enqueue(PostPublishedEvent event);
}
