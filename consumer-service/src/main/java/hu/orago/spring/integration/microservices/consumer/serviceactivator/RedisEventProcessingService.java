package hu.orago.spring.integration.microservices.consumer.serviceactivator;

import hu.orago.spring.integration.microservices.consumer.model.PostPublishedEvent;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

@Service
public class RedisEventProcessingService {

    @ServiceActivator(inputChannel = "eventChannel", poller = @Poller)
    public void process(PostPublishedEvent event) {
        System.out.println("megerkezett az event: " + event);
    }
}
