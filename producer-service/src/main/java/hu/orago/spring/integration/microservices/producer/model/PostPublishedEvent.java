package hu.orago.spring.integration.microservices.producer.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class PostPublishedEvent {

    private String postUrl;
    private String postTitle;
    private List<String> emails;

}
